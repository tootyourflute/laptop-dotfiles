alias cls='/usr/bin/clear'
alias l='/bin/ls -lAh --color=auto'
alias vim='/usr/bin/nvim'
alias wget='/usr/bin/wget --hsts-file="$XDG_CACHE_HOME/wget-hsts"'
alias todo='/usr/bin/nvim ~/Documents/todolist.txt'
alias wine32='WINEPREFIX=$HOME/.wine32 WINEARCH=win32'
alias i3config='vim ~/.config/i3/config'
function sl() {
	echo 'Did you mean `ls`? Here, I'\''ll fix that for ya'
	/bin/ls --color=auto
}
function scrot() { /usr/bin/scrot -d 1 '%Y-%m-%d-%H-%M-%S.png' -e 'mv $f ~/Pictures/screenshots/' }
