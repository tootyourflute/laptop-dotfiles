#!/bin/sh

batpercent_file=/tmp/batpct.txt
bat_percent=$(cat "$batpercent_file")
sec=10

countdown() {
	while [ $sec -ge 0 ]; do
		notify-send -u critical -t 1000 "Battery" "Shutting down in $sec"
		let "sec=sec-1"
		sleep 1
	done
}

while :
do
	if [[ $bat_percent -eq 15 ]]; then
		notify-send -u critical "Battery" "BAT0 at 15%"
	fi

	if [[ $bat_percent -lt 15 ]] && [[ $bat_percent -gt 5 ]]; then
		notify-send -u critical "Battery" "BAT0 less than 15%"
	fi

	if [[ $bat_percent -eq 5 ]]; then
		notify-send -u critical "Battery" "BAT0 at 5%, automatically shutting down"
		countdown
		shutdown now
	fi

	sleep 60
done
