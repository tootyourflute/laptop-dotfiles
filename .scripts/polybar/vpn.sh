#!/bin/bash

if [[ -n `ps -e | awk /openvpn/'{print $4}'` ]]
then
	echo -e %{F#0f0}VPN $(curl -s ipinfo.io/ip)
else
	echo -e %{F#f00}VPN
fi
