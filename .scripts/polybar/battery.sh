#!/bin/sh

ac_dir='/sys/class/power_supply/AC'
bat_dir='/sys/class/power_supply/BAT0'

bat_level=0
bat_max=0

if [ ! -d "$bat_dir" ]
then
	echo "AC"
fi

if [ -f "$bat_dir/charge_now" ]
then
	bat_level=$(cat "$bat_dir/charge_now")
fi

if [ -f "$bat_dir/charge_full" ]
then
	bat_max=$(cat "$bat_dir/charge_full")
fi

bat_percent=$(("$bat_level * 100 / $bat_max"))
charging=$(cat "$ac_dir/online")

if [ $bat_percent -ge 75 ]
then
	icon=""
fi
if [ $bat_percent -lt 75 ] && [ $bat_percent -ge 50 ]
then
	icon=""
fi
if [ $bat_percent -lt 50 ] && [ $bat_percent -ge 25 ]
then
	icon=""
fi
if [ $bat_percent -lt 25 ] && [ $bat_percent -ge 10 ]
then
	icon=""
fi
if [ $bat_percent -lt 10 ] && [ $bat_percent -ge 5 ]
then
	icon=""
fi
if [ $charging -eq 1 ]
then
	prefix=" "
else
	prefix=""
fi

echo "$prefix$icon $bat_percent%"
echo "$bat_percent" > /tmp/batpct.txt
