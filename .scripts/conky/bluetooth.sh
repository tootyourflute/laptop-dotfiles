#!/bin/bash

blue_service_status=`systemctl status bluetooth | awk /Active/'{print $2}'`
blue_status=`hcitool con | awk 'NR==2 {print}'`

blue_device() {
	if [ "${blue_status:-0}" != 0 ]
	then
		hcitool name $(echo $blue_status | awk '{print $3}')
	else
		echo "No device connected"
	fi
}

if [ $blue_service_status == 'inactive' ]
then
	echo "Bluetooth disabled"
else
	blue_device
fi
