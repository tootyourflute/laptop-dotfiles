#!/bin/bash

if [[ -n `ps -e | awk /openvpn/'{print $4}'` ]]
then
	echo "VPN active"
	exit 0
else
	echo "VPN not active"
	exit 0
fi
